var RSS = require('rss');
var writeFile = require('write');
var request = require('request');
var iconv  = require('iconv-lite');
var cheerio = require('cheerio')
var CronJob = require('cron').CronJob;
var currentTime = new Date();
var cleaner = require('clean-html');
const beautify = require('beautify');
var express = require('express')
var app = express()
const nodemailer = require('nodemailer');
var entities = require("entities");
var dotenv = require('dotenv').load();
// var winston = require('winston');

var currentTime = new Date();
var content = "";

/* lets create an rss feed */
var feed = new RSS({
  title: 'SF Bioklubben - Nyheter',
  description: 'Här samlas alla nyheter som SF skriver om till oss bioklubbsmedlemmar.\nRSS skapad med ♥ av Anton Olsson (anton-olsson@outlook.com)',
  feed_url: 'http://localhost:3000/',
  site_url: 'http://www3.sf.se/BIOKLUBBEN/',
  image_url: 'http://www.google.com/s2/favicons?domain=sf.se',
  pubDate: currentTime,
  language: 'sv',
  ttl: '60',
});

var requestOptions = { encoding: 'utf8', method: "GET", uri: 'http://www3.sf.se/BIOKLUBBEN/'};
request(requestOptions, function (error, response, body) {
  if (!error && response.statusCode == 200) {

    let $ = cheerio.load(body, { decodeEntities: false })

    /* loop over data and add to feed */
    $(' ul#NewsListingPage li').each(function(){
      let href = 'http://www.sf.se' + $(this).find('a.sfRedUrl').attr('href');
      let title = $(this).find('a.sfRedUrl h2').html()
          title = entities.decodeXML(title);
      let desc = $(this).find('div.newsListingContentText a p').html()
      let date = $(this).find('div.newsListingDateSecondary').html();

      let monthObj = {"januari": 1,"februari": 2,"mars": 3,"april": 4,"maj": 5,"juni": 6,"juli": 7,"augusti": 8,"september": 9,"oktober": 10,"november": 11,"december": 12}
      let dateArr = date.split(" ");
      let day = dateArr[0].replace(":e","")
      let month = monthObj[ dateArr[1] ]
      var d = new Date(`2017/${month}/${day}`);
      let imgLink = $(this).find('div.newsListingPictureSecondary a img').attr('src');



      feed.item({
          title:  title,
          description: desc,
          url: href, // link to the item
          date: d, // any format that js Date can parse.
      });
    })

    // cache the xml to send to clients
    var xml = feed.xml()
    xml = beautify(xml, {format: 'xml'});
    content = xml;
  }
})




app.get('/sf', function (req, res) {
  res.set('Content-Type', 'application/rss+xml');

  // winston.log('info', 'Hello!'); // winston.log('debug', 'Now my debug messages are written to console!');
  res.send(content)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})