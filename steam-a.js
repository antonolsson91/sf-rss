var RSS = require('rss');
var writeFile = require('write');
var request = require('request');
var iconv  = require('iconv-lite');
var cheerio = require('cheerio')
var CronJob = require('cron').CronJob;
var currentTime = new Date();
var cleaner = require('clean-html');
const beautify = require('beautify');
var express = require('express')
var app = express()
const nodemailer = require('nodemailer');
var entities = require("entities");
var dotenv = require('dotenv').load();
// var winston = require('winston');

var currentTime = new Date();

var se_monthObject = {"januari": 1,"februari": 2,"mars": 3,"april": 4,"maj": 5,"juni": 6,"juli": 7,"augusti": 8,"september": 9,"oktober": 10,"november": 11,"december": 12}
var se_monthObj = {"jan": 1,"feb": 2,"mar": 3,"apr": 4,"maj": 5,"jun": 6,"jul": 7,"aug": 8,"sep": 9,"okt": 10,"nov": 11,"dec": 12}
var en_monthObject = {"january": 1,"february": 2,"mars": 3,"april": 4,"may": 5,"june": 6,"july": 7,"august": 8,"september": 9,"october": 10,"november": 11,"december": 12}
var en_monthObj = {"jan": 1,"feb": 2,"mar": 3,"apr": 4,"may": 5,"jun": 6,"jul": 7,"aug": 8,"sep": 9,"oct": 10,"nov": 11,"dec": 12}

var content = "";


/* lets create an rss feed */
var feed = new RSS({
  title: 'SF Bioklubben - Nyheter',
  description: 'Här samlas alla nyheter som SF skriver om till oss bioklubbsmedlemmar.\nRSS skapad med ♥ av Anton Olsson (anton-olsson@outlook.com)',
  feed_url: 'http://localhost:3000/',
  site_url: 'http://www3.sf.se/BIOKLUBBEN/',
  image_url: 'http://www.google.com/s2/favicons?domain=sf.se',
  pubDate: currentTime,
  language: 'sv',
  ttl: '60',
});

scrapUrl = 'http://store.steampowered.com/news/?appids=457140';
var requestOptions = { encoding: 'utf8', method: "GET", uri: scrapUrl};

request(requestOptions, function (error, response, body) {
  if (!error && response.statusCode == 200) {

    let $ = cheerio.load(body, { decodeEntities: false })

    /* loop over data and add to feed */
    $(' div#news div[id*="post_"]').each(function(){
      let href = $(this).find('div.posttitle a').attr('href');
      let title = $(this).find('div.posttitle a').html()
      let desc = $(this).find('div.body').html()
      let date = $(this).find('div.date').html();
      let imgLink = $(this).find('img.capsule').attr('src');


      // 4 aug
      let dateArr = date.split(" ");
      let day = dateArr[0].replace(":e","")
      let month = en_monthObj[ dateArr[1] ]
      var d = new Date();
      d.setUTCMonth(month-1)
      d.setUTCDate(day)

      feed.item({
          title:  entities.decodeXML( title ),
          description: desc,
          url: href, // link to the item
          date: d, // any format that js Date can parse.
      });
    })

    // cache the xml to send to clients
    var xml = feed.xml()
    xml = beautify(xml, {format: 'xml'});
    content = xml;
  }
})




app.get('/oni', function (req, res) {
  res.set('Content-Type', 'application/rss+xml');

  // winston.log('info', 'Hello!'); // winston.log('debug', 'Now my debug messages are written to console!');
  res.send(content)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})