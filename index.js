var RSS = require('rss');
var writeFile = require('write');
var request = require('request');
var iconv  = require('iconv-lite');
var cheerio = require('cheerio')
var CronJob = require('cron').CronJob;
var currentTime = new Date();
var cleaner = require('clean-html');
const beautify = require('beautify');
var express = require('express')
var app = express()
const nodemailer = require('nodemailer');
var entities = require("entities");
var dotenv = require('dotenv').load();
var winston = require('winston');

function requireEnvVar(name) {
	var val = process.env[name];
	if (typeof val !== 'string') {
		throw new Error(`Required environment variable ${name} is not set`);
	}
	return val;
}

winston.add(winston.transports.File, { filename: 'access.log' });

var gmailUser = requireEnvVar('GMAIL_USER')
var gmailPwd = requireEnvVar('GMAIL_PWD')
var once = false;


/*! ******************************** !*/


var rss = function(){
  currentTime = new Date();

  /* lets create an rss feed */
  var feed = new RSS({
    title: 'SF Bioklubben - Nyheter',
    description: 'Här samlas alla nyheter som SF skriver om till oss bioklubbsmedlemmar.\nRSS skapad med ♥ av Anton Olsson (anton-olsson@outlook.com)',
    feed_url: 'http://localhost:3000/',
    site_url: 'http://www3.sf.se/BIOKLUBBEN/',
    image_url: 'http://www.google.com/s2/favicons?domain=sf.se',
    pubDate: currentTime,
    language: 'sv',
    ttl: '60',
  });

  var requestOptions = { encoding: 'utf8', method: "GET", uri: 'http://www.sf.se/BIOKLUBBEN/'};
  request(requestOptions, function (error, response, body) {
    if (!error && response.statusCode == 200) {

      let $ = cheerio.load(body, { decodeEntities: false })

      /* loop over data and add to feed */
      $(' ul#NewsListingPage li').each(function(){
        let href = 'http://www.sf.se' + $(this).find('a.sfRedUrl').attr('href');
        let title = $(this).find('a.sfRedUrl h2').html()
        let desc = $(this).find('div.newsListingContentText a p').html()
        let date = $(this).find('div.newsListingDateSecondary').html();

        if( title.toLowerCase().includes("förhandsvisning") || desc.toLowerCase().includes("förhandsvisning") && !once ){
          once = true;

          // setup email data with unicode symbols
          let mailOptions = {
              from: `"Anton @ Node 🤖" <${gmailUser}>`, // sender address
              to: `${gmailUser}`, // list of receivers
              subject: entities.decodeXML(title), // Subject line
              html: `
              <b>En ny förhandsvisning verkar ha uppenbarat sig!</b>
              <hr>${desc}
              ` // html body
          };

          console.log(JSON.stringify(mailOptions, null, '\t'));

          /*

          // send mail with defined transport object
          transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  return console.log(error);
              }
              console.log('Message %s sent: %s', info.messageId, info.response);
          });

          */


        }

        let monthObj = {"januari": 1,"februari": 2,"mars": 3,"april": 4,"maj": 5,"juni": 6,"juli": 7,"augusti": 8,"september": 9,"oktober": 10,"november": 11,"december": 12}
        let dateArr = date.split(" ");
        let day = dateArr[0].replace(":e","")
        let month = monthObj[ dateArr[1] ]
        var d = new Date(`2017/${month}/${day}`);
        let imgLink = $(this).find('div.newsListingPictureSecondary a img').attr('src');

        feed.item({
            title:  title,
            description: desc,
            url: href, // link to the item
            date: d, // any format that js Date can parse.
        });
      })

			console.log(feed.xml());

      // cache the xml to send to clients
      var xml = feed.xml()
      xml = beautify(xml, {format: 'xml'});

			console.log(entities.decode(xml));
			console.log(entities.decode('&lt;&gt;&quot;&amp;&copy;&reg;')); // <>"&©®

      app.get('/', function (req, res) {
        res.set('Content-Type', 'application/rss+xml; charset=utf-8');

				winston.log('info', 'Hello!');

				// winston.log('debug', 'Now my debug messages are written to console!');

        res.send(xml)

      })


    }
  })
}

rss()

var job = new CronJob('00 00 * * * *', function() {
  /*
   * Varje dag, varje timme.
   */
  rss()

  currentTime = new Date();
  console.log(`${currentTime} Cronjob executed.`);

  }, function () {
    /* This function is executed when the job stops */
    console.log(`${currentTime} Cronjob stopped!`);
  },
  true, /* Start the job right now */
  'Europe/Stockholm' /* Time zone of this job. */
);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})